<?php
$real_path = str_replace("\\", "/", dirname(__FILE__));

define('DB_HOST', '127.0.0.1'); 	// 数据库服务器地址
define('DB_USER', 'root');  		// 数据库用户名
define('DB_PWD', 'root');			// 数据库密码
define('DB_NAME', 'test');  		// 数据库名称
define('DB_PORT', '3306');  		// 数据库端口

require_once $real_path . '/M.class.php';
require_once $real_path . '/MysqliDb.class.php';